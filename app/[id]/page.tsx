import { getFirst, getPostIdList, getImages} from "@/engine/getPosts";
import { Post } from "@/engine/types";
import Header from "../components/header";
import { GetDate } from "../components/getDate";
import Link from "next/link";
import { Gallery } from "../components/gallery";
import { FormatDescription } from "@/engine/formatDescription";

export async function getStaticPaths() {
    const paths = await getPostIdList();
    return {
        paths,
        fallback: false
    }
}

export default async function Post({params: {id}}: {params: {id:string}}) {
    const post = await getFirst(parseInt(id)) as Post;
    const images = await getImages(parseInt(id));
    

    return (
        <>
            <Header />
            <main className="p-4">
                <h1 className="text-xl mb-4">{post.title}</h1>
                <div className="mb-4 flex gap-4">
                    <Gallery images={images}/>
                </div>
                <div className="mb-4"><FormatDescription content={post.description} /></div>
                <div>Date aquired: <GetDate date={post.aquired} /></div>
                <div className="mb-4">
                    Last edited: <GetDate date={post.edited} />
                </div>
                <div>
                    <Link href={`/${id}/edit`} className="p-2 border rounded hover:bg-slate-800">Edit Post</Link>
                </div>
            </main>
        </>
    )
}