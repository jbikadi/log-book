import Header from "@/app/components/header";
import PostForm from "../../components/form";
import editPost from "@/engine/editPost";
import { getFirst, getImages, getPostIdList } from "@/engine/getPosts";
import { Post } from "@/engine/types";

// return a 404 page if post does not exist in db
export async function getStaticPaths() {
    const paths = await getPostIdList();
    return {
        paths,
        fallback: false
    }
}

export default async function Edit({params: {id}}: {params: {id:string}}) {
    const formValues = await getFirst(parseInt(id)) as Post;
    const images = await getImages(parseInt(id));
    const paths = images.map((e) => e.path);

    return (
        <>
            <Header />
            <main className="p-4">
                <h1>Edit Post {id}</h1>
                <PostForm Post={editPost} values={formValues} imagePaths={paths} />
            </main>
        </>
    )
}