import Header from './components/header';
import { Pager } from './components/pager';
import Posts from './components/posts';
import { getPosts } from '@/engine/getPosts';

export default async function Home(params: {searchParams: {page: string}}) {
    const modifier = 4;
    const page = parseInt(params.searchParams.page) || 0;
    const posts = await getPosts();
    const base = modifier * page;

    return(
        <>
            <Header />
            <main className="p-4">
                <div className="grid gap-4 grid-cols-single md:grid-cols-double">
                    {posts.filter((_, i) => i >= base && i < base + modifier).map((post: {id: number}) => (
                        <Posts key={post.id} post={post} />
                    ))}
                </div>

                <Pager pages={posts.length} modifier={modifier} page={page} />
            </main>
        </>
    );
}
