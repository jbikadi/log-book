'use client';

import { useState } from "react";
import { handleDragEnter, handleDragLeave, handleDragOver, handleDrop, handleFileSelect } from "@/engine/form";
import { useDispatch } from "react-redux";
import { Post } from "@/engine/types";
import Image from "next/image";

interface FormPost {
    Post: (data: FormData) => Promise<void>;
    values?: Post;
    imagePaths?: string[];
}

export default function EditForm({Post, values, imagePaths}: FormPost) {
    const [images, setImages] = useState<File[]>(() => []);
    const [paths, setPaths] = useState(imagePaths || []);
    const [removeImages, setRemoveImages] = useState<string[]>([]);
    const dispatch = useDispatch();
    const date = values?.aquired.toISOString().substring(0,10);

    const haneldPaths = (path: string) => {
        setRemoveImages((e) => [...e, path]);
        setPaths((a) => a.filter((e) => e !== path));
    }

    const handleSubmit = (data: FormData) => {
        const formData = data;
        formData.delete('files');
        if (typeof values?.id === 'number') {
            formData.append('id', values.id.toString());
        }
        removeImages.forEach((e) => {
            formData.append('imagePaths', e);
        });
        images.forEach((e: File) => {
            formData.append('images', e);
        });

        Post(formData);
    }

    return (
        <form action={handleSubmit} className="flex gap-2 flex-col">
            <label htmlFor="title">Title or Name:</label>
            <input type="text" name="title" id="title" defaultValue={values?.title} className="text-gray-950" />
            <label htmlFor="description">Description or Comments: </label>
            <textarea
                name="description"
                id="description"
                className="text-gray-950"
                defaultValue={values?.description}
            ></textarea>
            <label htmlFor="aquired">Date aquired:</label>
            <input type="date" name="aquired" id="aquired" defaultValue={date} className="text-gray-950" />
            <label htmlFor="files">Image</label>
            <div
                className="border rounded flex justify-center w-40"
                onDragEnter={(e) => handleDragEnter(e, dispatch)}
                onDragOver={(e) => handleDragOver(e, dispatch)}
                onDragLeave={(e) => handleDragLeave(e, dispatch)}
                onDrop={(e) => handleDrop(e, dispatch, images, setImages)}
            >
                drag and drop
            </div>
            <input
                type="file"
                name="files"
                id="files"
                multiple
                onChange={(e) => handleFileSelect(e, images, setImages)}
            />
            <div className="flex flex-col md:flex-row gap-4">
                {images.length > 0 &&
                    <ul className="flex-1">
                        {images.map((image: File) => {
                            return (
                                <li key={image.name}>{image.name}</li>
                            );
                        })}
                    </ul>
                }
                {imagePaths &&
                    <div className="flex-1 flex gap-4">
                        {paths.map((e) =>
                            <div key={e} className="relative w-20 h-20">
                                <Image src={e} alt="thumbnail" fill={true} />
                                <button
                                    onClick={() => haneldPaths(e)}
                                    className="absolute w-full h-full cursor-pointer bg-slate-900/75"
                                ><span>delete image</span></button>
                            </div>
                        )}
                    </div>
                }
            </div>
            <input type="submit" value="Submit" className="border rounded cursor-pointer hover:bg-slate-900 active:bg-slate-700" />
        </form>
    );
}