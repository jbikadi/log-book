import Link from "next/link";

export default function Header() {
    return (
        <nav className="p-4 bg-gray-800 flex justify-between items-center">
            <Link href="/">Home</Link>
            <Link href="/new" className="border p-2 rounded hover:bg-slate-700">New Post</Link>
        </nav>
    )
}