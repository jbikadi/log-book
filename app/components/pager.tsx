import Link from "next/link";

export function Pager({pages, modifier, page}: {pages:number, modifier: number, page: number}) {
    const pager = Array.from(Array(Math.ceil(pages / modifier)).keys());

    if (pager.length <= 1) {
        return (<></>)
    }

    return (
        <div className="flex gap-4 justify-center items-center py-4">
            {page > 0 && 
                <Link href={{query: {page: page - 1}}} className="border rounded px-2 py-1 hover:bg-slate-800">Prev</Link>
            }
            {pager.map((e, i) => (
                <>
                    {i == page ?
                        <span className="border rounded px-2 py-1 text-slate-900 bg-slate-100">{e}</span>
                        :
                        <Link href={{query: {page: e}}} className="border rounded px-2 py-1 hover:bg-slate-800">{e}</Link>
                    }
                </>
            ))}
            {page + 1 < pager.length && 
                <Link href={{query: {page: page + 1}}} className="border rounded px-2 py-1 hover:bg-slate-800">Next</Link>
            }
        </div>
    )
}