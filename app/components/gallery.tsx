'use client';

import Image from "next/image";
import { useState } from "react";

type Images = {
    id: number;
    imageId: number;
    path: string;
}[];

export function Gallery({images}: {images: Images}) {
    const [thumb, setThumb] = useState(0);

    if (images.length < 1) {
        return (<div className="thumbnail-container"></div>);
    }

    return (
        <>
            <figure className="">
                {images.map((e, i) =>
                    <div key={e.id} className={i === thumb ? 'block' : 'hidden'}>
                        <Image
                            src={e.path}
                            alt={`image ${e.id}`}
                            height={200}
                            width={200}
                        />
                    </div>
                )}
            </figure>
            <div className="flex flex-col gap-4">
                {images.map((e, i) =>
                    <div key={e.id} className="relative w-12 h-12">
                        <button onClick={() => setThumb(() => i)}>
                            <Image
                                src={e.path}
                                alt={`thumbnail ${e.id}`}
                                fill={true}
                            />
                        </button>
                    </div>
                )}
            </div>
        </>
    )
}