
export function GetDate({date}: {date: Date}) {
    const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    return (
        <time dateTime={date.toString()} className="text-gray-200">
            {months[date.getMonth()]} {date.getDate()}, {date.getFullYear()}
        </time>
    )
}