import Image from "next/image";
import { Post } from '@/engine/types';
import { GetDate } from "./getDate";
import Link from "next/link";
import { FormatDescription } from "@/engine/formatDescription";
import { getFirst, getThumbnail } from "@/engine/getPosts";

export default async function Posts({post: {id}}: {post: {id: number}}) {
    const {title, description, aquired} = await getFirst(id) as Post;
    const thumbnail = await getThumbnail(id);

    return (
        <>
            <div className="w-24 h-24 relative">
                {thumbnail ?
                    <Image
                        src={thumbnail.path}
                        alt={'thumbnail'}
                        fill={true}
                        style={{objectFit: "cover"}}
                    />
                    :
                    <div className="thumbnail-container"></div>
                }
            </div>
            <div className="px-4">
                <h2 className="line-clamp-1">{title}</h2>
                <div className="line-clamp-2">
                    <FormatDescription content={description} />
                </div>
                <GetDate date={aquired} />
            </div>
            <div>
                <Link href={'/' + id} className="border rounded p-2 hover:bg-slate-800">View <span className="sr-only">{title}</span></Link>
            </div>
        </>
    );
}