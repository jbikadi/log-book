import createPost from "@/engine/createPost";
import PostForm from "../components/form";
import Header from "../components/header";

export default function Page() {

    return (
        <>
            <Header />
            <main className="p-4">
                <header>
                    <h1>New</h1>
                </header>
                <PostForm Post={createPost} />
            </main>
        </>
    )
}