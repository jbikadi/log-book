
export function FormatDescription({content}: {content:string}) {
    const r = content.trim().split(/\r?\n/).filter((e) => e.trim().length > 0);
    return (
        <>
            {r.map((e, i) => <p key={i}>{e}</p>)}
        </>
    )
}