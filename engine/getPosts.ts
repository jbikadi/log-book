import { prisma } from "./db";

export function getPosts() {
    const query = {
        select: {
            id: true
        }
    }
    return prisma.post.findMany(query);
}

export function getFirst(id: number) {
    const query = {
        where: {
            id
        }
    };
    return prisma.post.findUnique(query);
}

export function getImages(id: number) {
    const query = {
        where: {
            imageId: id
        }
    };
    return prisma.images.findMany(query);
}

export function getThumbnail(id: number) {
    const query = {
        where: {
            imageId: id
        },
        select: {
            path: true
        }
    }
    return prisma.images.findFirst(query)
}

// getStaticPaths return string for html generation
export async function getPostIdList() {
    const posts = await getPosts();
    return posts.map((e) => {
        return {params: {id: e.id.toString()}}
    });
}
