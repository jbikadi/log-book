import fs from "fs";
import { prisma } from "@/engine/db";

async function createPost(data: FormData) {
    "use server"

    const title = data.get("title")?.valueOf();
    const description = data.get("description")?.valueOf();
    const aquired = data.get("aquired")?.valueOf();
    const images = Array.from(data.values());

    if (typeof title !== 'string' || typeof description !== 'string' || typeof aquired !== 'string') {
        throw new Error('invalid field');
    }

    const post = await prisma.post.create({
        data: {
            title,
            description,
            aquired: new Date(aquired).toISOString()
        }
    });

    for (const image of images) {
        if (typeof image === 'object' && 'arrayBuffer' in image) {
            const file = image as unknown as Blob;
            const buffer = Buffer.from(await file.arrayBuffer());
            if (!fs.existsSync(`public/uploads/${post.id}`)) {
                fs.mkdirSync(`public/uploads/${post.id}`);
            }
            fs.writeFileSync(`public/uploads/${post.id}/${file.name}`, buffer);
            const postImages = await prisma.images.create({
                data: {
                    imageId: post.id,
                    path: `/uploads/${post.id}/${file.name}`
                }
            });
        }
    }

}

export default createPost;