import { createSlice, type PayloadAction } from '@reduxjs/toolkit';

interface ReducerActions {
    inDropZone: boolean;
    fileList: File[];
}

const initialState: ReducerActions = {
    inDropZone: false,
    fileList: []
};

const formSlice = createSlice({
    name: 'form',
    initialState,
    reducers: {
        SET_DROP_ZONE: (state, action: PayloadAction<boolean>) => {
            return {...state, inDropZone: action.payload};
        },
        // Note: this will never be used as it throws errors 
        //       due to redux not being designed to hold File types
        ADD_FILE: (state, action: PayloadAction<File[]>) => {
            return {...state, fileList: state.fileList.concat(action.payload)};
        }
    }
});

export const { SET_DROP_ZONE } = formSlice.actions;

export default formSlice.reducer;