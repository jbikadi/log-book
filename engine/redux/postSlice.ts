import { createSlice, type PayloadAction } from '@reduxjs/toolkit';

// TODO: fix the images type
type Post = {
    id?: number;
    title: string;
    description: string;
    images?: any;
    aquired: Date | null;
    created?: Date;
    edited?: Date;
};

const initialState: Post = {
    title: '',
    description: '',
    images: {},
    aquired: null
};

const postSlice = createSlice({
    name: 'post',
    initialState,
    reducers: {
        newPost: (state) => {
            return state;
        },
        editPost: (state, action: PayloadAction) => {
            return state;
        }
    }
});

export const { newPost, editPost } = postSlice.actions;

export default postSlice.reducer;