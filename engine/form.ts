import { ChangeEvent, Dispatch, SetStateAction } from "react";
import { AppDispatch } from "./redux/store";
import { SET_DROP_ZONE } from "@/engine/redux/formSlice";


// onDragEnter set inDropZone to true
export const handleDragEnter = (e: React.DragEvent<HTMLDivElement>, dispatch: AppDispatch) => {
    e.preventDefault();
    e.stopPropagation();

    dispatch(SET_DROP_ZONE(true));
}

// onDragLeave sets inDropZone to false
export const handleDragLeave = (e: React.DragEvent<HTMLDivElement>, dispatch: AppDispatch) => {
    e.preventDefault();
    e.stopPropagation();
    dispatch(SET_DROP_ZONE(false));
}

// onDragOver sets inDropZone to true
export const handleDragOver = (e: React.DragEvent<HTMLDivElement>, dispatch: AppDispatch) => {
    e.preventDefault();
    e.stopPropagation();
    e.dataTransfer.dropEffect = "copy";
    dispatch(SET_DROP_ZONE(true));
}

// onDrop sets inDropZone to false and adds images to fileList
export const handleDrop = (
    e: React.DragEvent<HTMLDivElement>,
    dispatch: AppDispatch,
    images: File[],
    setImages: Dispatch<SetStateAction<File[]>>
) => {
    e.preventDefault();
    e.stopPropagation();

    // get files from event on dataTransfer object and converts to an array
    let files = Array.from(e.dataTransfer.files || []);

    if (files && files.length > 0) {
        const existingFiles = images.map((f: File) => f.name);

        files = files.filter((f) =>
            !existingFiles.includes(f.name) && 
            (f.type === 'image/png' || f.type === 'image/jpeg')
        );

        setImages((i) => i.concat(files));
        dispatch(SET_DROP_ZONE(false));
    }
}

// browse files and adds images to fileList
export const handleFileSelect = (
    e: ChangeEvent<HTMLInputElement>,
    images: File[],
    setImages: Dispatch<SetStateAction<File[]>>
) => {
    // get files from event on files object and converts to an array
    let files = Array.from(e.target.files || []);

    if (files && files.length > 0) {
        const existingFiles = images.map((f) => f.name);

        files = files.filter((f) =>
            !existingFiles.includes(f.name) && 
            (f.type === 'image/png' || f.type === 'image/jpeg')
        );

        setImages((i) => i.concat(files));
    }
}