
export interface ReducerActions {
    type: Actions;
    inDropZone?: boolean;
    files?: File[];
}

export enum Actions {
    SET_DROP_ZONE,
    ADD_FILE
}

export const reducer = (state: any, action: ReducerActions) => {
    switch(action.type) {
        case Actions.SET_DROP_ZONE:
            return {...state, inDropZone: action.inDropZone};
        case Actions.ADD_FILE:
            return {...state, fileList: state.fileList.concat(action.files)};
        default:
            return state;
    }
}