
export type Post = {
    id?: number;
    title: string;
    description: string;
    aquired: Date;
    created: Date;
    edited: Date;
};
